# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = "exception_notification_rails3"
  s.version = "1.2.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Jamis Buck", "Josh Peek"]
  s.date = "2010-11-03"
  s.email = "timocratic@gmail.com"
  s.homepage = "http://github.com/railsware/exception_notification"
  s.require_paths = ["lib"]
  s.rubygems_version = "1.8.25"
  s.summary = "Exception notification by email for Rails 3 apps"

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
    else
    end
  else
  end
end
