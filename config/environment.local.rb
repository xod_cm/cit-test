# encoding: UTF-8

$CONFIG = {
       :domain => "0.0.1",  # The client specific hostname will be prepended to this domain
       :email_domain => "0.0.1",
       :replyto => "admin",  # Note that this is not a full email address, just the part before the @
       :from => "admin",  # Note that this is not a full email address, just the part before the @
       :prefix => "[Jobsworth]",
       :productName => "Jobsworth",
       :SSL => false
}

ActionMailer::Base.smtp_settings = {
  :address  => "localhost",
  :port  => 25,
  :domain  => '0.0.1'
}

# Setup email notification of errors
Jobsworth::Application.config.middleware.use ExceptionNotifier,
    :email_prefix => "[Jobsworth error] ",
    :sender_address => %{"Jobsworth" <error@0.0.1>},
    :exception_recipients => %w{exceptions@example.com}
